
#include "pose_algebra/pose.h"




namespace Pose
{



Pose_::Pose_()
{

}

Pose_::~Pose_()
{

}





Pose2d_::Pose2d_()
{
    setZero();
}

Pose2d_::~Pose2d_()
{

}

void Pose2d_::setZero()
{
    position_.setZero();
    attitude_=0;
    scale_=1;

    return;
}





Pose3d_::Pose3d_()
{
    setZero();
}

Pose3d_::~Pose3d_()
{

}

void Pose3d_::setZero()
{
    position_.setZero();
    attitude_.setZero();
    attitude_(0)=1;
    scale_=1;

    return;
}








Pose2d& Pose2d::operator=(const Pose2d& other_pose)
{
    position_=other_pose.position_;
    attitude_=other_pose.attitude_;
    scale_=other_pose.scale_;
    frame_id_=other_pose.frame_id_;
    father_frame_id_=other_pose.father_frame_id_;
    return *this;
}

Pose2d Pose2d::operator+() const
{
    Pose2d result;

    result.position_=position_;
    result.attitude_=attitude_;
    result.scale_=scale_;
    result.frame_id_=frame_id_;
    result.father_frame_id_=father_frame_id_;

    return result;
}

Pose2d Pose2d::operator+(const Pose2d& pose) const
{
    Pose2d result;

    if(frame_id_ != pose.father_frame_id_)
    {
        std::cout<<"error in Pose::operator+"<<std::endl;
        throw 1;
    }

    // TODO
    result.position_;
    result.attitude_;
    result.scale_=scale_*pose.scale_;
    result.frame_id_=pose.frame_id_;
    result.father_frame_id_=father_frame_id_;


    return result;
}

Pose2d Pose2d::operator-() const
{
    Pose2d result;

    // TODO
    result.position_;
    result.attitude_;
    result.scale_=1/scale_;
    result.frame_id_=father_frame_id_;
    result.father_frame_id_=frame_id_;

    return result;
}

Pose2d Pose2d::operator-(const Pose2d& pose) const
{
    Pose2d result;

    try
    {
        result=(*this)+(pose.operator-());
    }
    catch(...)
    {
        std::cout<<"error in Pose::operator-"<<std::endl;
        throw 1;
    }

    return result;
}

bool Pose2d::isCorrect() const
{
    // check attitude
    if(std::isnan(attitude_))
        return false;
    if(std::isinf(attitude_))
        return false;

    // check position
    for(int i=0; i<2; i++)
    {
        if(std::isnan(position_(i)))
            return false;
        if(std::isinf(position_(i)))
            return false;
    }

    return true;
}










Pose3d& Pose3d::operator=(const Pose3d& other_pose)
{
    position_=other_pose.position_;
    attitude_=other_pose.attitude_;
    scale_=other_pose.scale_;
    frame_id_=other_pose.frame_id_;
    father_frame_id_=other_pose.father_frame_id_;
    return *this;
}

Pose3d Pose3d::operator+() const
{
    Pose3d result;

    result.position_=position_;
    result.attitude_=attitude_;
    result.scale_=scale_;
    result.frame_id_=frame_id_;
    result.father_frame_id_=father_frame_id_;

    return result;
}

Pose3d Pose3d::operator+(const Pose3d& pose) const
{
    Pose3d result;

    if(frame_id_ != pose.father_frame_id_)
    {
        std::cout<<"error in Pose::operator+"<<std::endl;
        throw 1;
    }

    result.position_=Quaternion::cross_sandwich(attitude_, pose.position_, Quaternion::conj(attitude_))+pose.scale_*position_;
    result.attitude_=Quaternion::cross(attitude_,pose.attitude_);
    result.scale_=scale_*pose.scale_;
    result.frame_id_=pose.frame_id_;
    result.father_frame_id_=father_frame_id_;


    return result;
}

Pose3d Pose3d::operator-() const
{
    Pose result;

    result.position_=-1/scale_*Quaternion::cross_sandwich(Quaternion::conj(attitude_), position_, attitude_);
    result.attitude_=Quaternion::conj(attitude_);
    result.scale_=1/scale_;
    result.frame_id_=father_frame_id_;
    result.father_frame_id_=frame_id_;

    return result;
}

Pose3d Pose3d::operator-(const Pose3d& pose) const
{
    Pose3d result;

    try
    {
        result=(*this)+(pose.operator-());
    }
    catch(...)
    {
        std::cout<<"error in Pose::operator-"<<std::endl;
        throw 1;
    }

    return result;
}

bool Pose3d::isCorrect() const
{
    // check attitude
    for(int i=0; i<4; i++)
    {
        if(attitude_(i)>1+TOLERANCE || attitude_(i)<-(1+TOLERANCE))
            return false;
        if(std::isnan(attitude_(i)))
            return false;
        if(std::isinf(attitude_(i)))
            return false;
    }
    if(std::abs(attitude_.norm()-1.0) > TOLERANCE)
        return false;

    // check position
    for(int i=0; i<3; i++)
    {
        if(std::isnan(position_(i)))
            return false;
        if(std::isinf(position_(i)))
            return false;
    }


    return true;
}

}
