

#ifndef _POSE_H_
#define _POSE_H_




//I/O stream
//std::cout
#include <iostream>

//String
//std::string, std::getline()
#include <string>

//String stream
//std::istringstream
#include <sstream>

//File Stream
//std::ofstream, std::ifstream
#include <fstream>


//Vector
//std::vector
#include <vector>

// List
#include <list>


// Exceptions
#include <exception>

// Math
#include <cmath>


// Eigen
#include <Eigen/Dense>

// Quaternion algebra
#include <quaternion_algebra/quaternion_algebra.h>


namespace Pose
{

const double TOLERANCE=1e-3;




//////////////////////
/// \brief The Pose_ class
///
class Pose_
{
public:
    Pose_();
    virtual ~Pose_();


public:
    std::string frame_id_;
    std::string father_frame_id_;



public:
    virtual void setZero()=0;

public:
    virtual bool isCorrect() const=0;



};






//////////////////////////////
/// \brief The Pose2d_ class
///
class Pose2d_ : public Pose_
{
public:
    Pose2d_();
    virtual ~Pose2d_();

public:
    Eigen::Vector2d position_;
    double attitude_;
    double scale_;

public:
    void setZero();
};



//////////////////////////////
/// \brief The Pose3d_ class
///
class Pose3d_ : public Pose_
{
public:
    Pose3d_();
    virtual ~Pose3d_();

public:
    Eigen::Vector3d position_;
    Eigen::Vector4d attitude_;
    double scale_;

public:
    void setZero();
};






////////////////////////
/// \brief The Pose2d class
///
class Pose2d : public Pose2d_
{

    // TODO
public:
    Pose2d& operator=(const Pose2d& other_pose);
    Pose2d operator+() const;
    Pose2d operator+(const Pose2d& pose) const;
    Pose2d operator-() const;
    Pose2d operator-(const Pose2d& pose) const;

public:
    bool isCorrect() const;

};





////////////////////////
/// \brief The Pose3d class
///
class Pose3d : public Pose3d_
{

public:
    Pose3d& operator=(const Pose3d& other_pose);
    Pose3d operator+() const;
    Pose3d operator+(const Pose3d& pose) const;
    Pose3d operator-() const;
    Pose3d operator-(const Pose3d& pose) const;

public:
    bool isCorrect() const;

};


////////////////
/// \brief Pose
///
typedef Pose3d Pose;



}




#endif
